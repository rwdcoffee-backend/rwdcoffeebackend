import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { SalaryService } from './Salary.service';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';

@Controller('Salary')
export class SalaryController {
  constructor(private readonly SalaryService: SalaryService) {}
  // Create
  @Post()
  create(@Body() createUserDto: CreateSalaryDto) {
    return this.SalaryService.create(createUserDto);
  }

  // Read All
  @Get()
  findAll() {
    return this.SalaryService.findAll();
  }
  // Read One
  @Get(':id')
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  findOne(@Param('id') id: string) {
    return this.SalaryService.findOne();
  }
  // Partial Update
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateSalaryDto) {
    return this.SalaryService.update(+id, updateUserDto);
  }
  // Delete
  @Delete(':id')
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  remove(@Param('id') id: string) {
    return this.SalaryService.remove();
  }
}
