import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product)
    private ProductRepository: Repository<Product>,
  ) {}

  create(createProductDto: CreateProductDto): Promise<Product> {
    return this.ProductRepository.save(createProductDto);
  }

  findAll(): Promise<Product[]> {
    return this.ProductRepository.find();
  }

  findOne(Id: number) {
    return this.ProductRepository.findOneBy({ Id });
  }

  async update(Id: number, updateProductDto: UpdateProductDto) {
    await this.ProductRepository.update(Id, updateProductDto);
    const Product = await this.ProductRepository.findOneBy({ Id });
    return Product;
  }

  async remove(Id: number) {
    const deleteProduct = await this.ProductRepository.findOneBy({ Id });
    return this.ProductRepository.remove(deleteProduct);
  }
}
