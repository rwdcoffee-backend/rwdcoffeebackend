import { Module } from '@nestjs/common';
import { MemberService } from './member.service';
import { MemberController } from './Member.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Member } from './entities/Member.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Member])],
  controllers: [MemberController],
  providers: [MemberService],
})
export class MemberModule {}
