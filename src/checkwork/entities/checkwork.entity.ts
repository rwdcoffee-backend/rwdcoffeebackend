import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class CheckWork {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  password: string;
  @Column()
  fullname: string;
  @Column()
  timein: string;
  @Column()
  timeout: string;
}
