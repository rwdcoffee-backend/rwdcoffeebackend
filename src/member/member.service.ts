import { Injectable } from '@nestjs/common';
import { CreateMemberDto } from './dto/create-member.dto';
import { UpdateMemberDto } from './dto/update-member.dto';
import { Member } from './entities/Member.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MemberService {
  constructor(
    @InjectRepository(Member)
    private MemberRepository: Repository<Member>,
  ) {}

  create(createMemberDto: CreateMemberDto): Promise<Member> {
    return this.MemberRepository.save(createMemberDto);
  }

  findAll(): Promise<Member[]> {
    return this.MemberRepository.find();
  }

  findOne(id: number) {
    return this.MemberRepository.findOneBy({ id });
  }

  async update(id: number, updateMemberDto: UpdateMemberDto) {
    await this.MemberRepository.update(id, updateMemberDto);
    const Member = await this.MemberRepository.findOneBy({ id });
    return Member;
  }

  async remove(id: number) {
    const deleteMember = await this.MemberRepository.findOneBy({ id });
    return this.MemberRepository.remove(deleteMember);
  }
}
