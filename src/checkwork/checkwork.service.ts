import { Injectable } from '@nestjs/common';
import { CreateCheckworkDto } from './dto/create-checkwork.dto';
import { UpdateCheckWorkDto } from './dto/update-checkwork.dto';
import { CheckWork } from './entities/checkwork.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class CheckWorkService {
  constructor(
    @InjectRepository(CheckWork)
    private CheckworkRepository: Repository<CheckWork>,
  ) {}

  create(createCheckWorkDto: CreateCheckworkDto): Promise<CheckWork> {
    return this.CheckworkRepository.save(createCheckWorkDto);
  }

  findAll(): Promise<CheckWork[]> {
    return this.CheckworkRepository.find();
  }

  findOne(id: number) {
    return this.CheckworkRepository.findOneBy({ id });
  }

  async update(id: number, updateCheckWorkDto: UpdateCheckWorkDto) {
    await this.CheckworkRepository.update(id, updateCheckWorkDto);
    const checkwork = await this.CheckworkRepository.findOneBy({ id });
    return checkwork;
  }

  async remove(id: number) {
    const deleteCheckWork = await this.CheckworkRepository.findOneBy({ id });
    return this.CheckworkRepository.remove(deleteCheckWork);
  }
}
