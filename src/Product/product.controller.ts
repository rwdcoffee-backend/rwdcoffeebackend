import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { ProductService } from './product.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';

@Controller('Product')
export class ProductController {
  constructor(private readonly ProductService: ProductService) {}
  // Create
  @Post()
  create(@Body() createUserDto: CreateProductDto) {
    return this.ProductService.create(createUserDto);
  }

  // Read All
  @Get()
  findAll() {
    return this.ProductService.findAll();
  }
  // Read One
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.ProductService.findOne(+id);
  }
  // Partial Update
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateProductDto: UpdateProductDto) {
    return this.ProductService.update(+id, updateProductDto);
  }
  // Delete
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.ProductService.remove(+id);
  }
}
