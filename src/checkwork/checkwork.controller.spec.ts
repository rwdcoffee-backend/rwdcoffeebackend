import { Test, TestingModule } from '@nestjs/testing';
import { CheckWorkController } from './checkwork.controller';
import { CheckWorkService } from './checkwork.service';

describe('UsersController', () => {
  let controller: CheckWorkController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CheckWorkController],
      providers: [CheckWorkService],
    }).compile();

    controller = module.get<CheckWorkController>(CheckWorkController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
