import { IsNotEmpty, Length } from 'class-validator';
export class CreateIngredientDto {
  @IsNotEmpty()
  @Length(4)
  IngredientID: string;

  @IsNotEmpty()
  @Length(4, 32)
  IngredientName: string;

  @IsNotEmpty()
  Minimum: number;

  @IsNotEmpty()
  Balance: number;

  @IsNotEmpty()
  Price: number;
}
