import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class paySalary {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  date: string;
  @Column()
  userid: number;
  @Column()
  workinghour: number;
  @Column()
  workrate: number;
}
