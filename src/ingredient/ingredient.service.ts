import { Injectable } from '@nestjs/common';
import { CreateIngredientDto } from './dto/create-ingredient.dto';
import { UpdateIngredientDto } from './dto/update-ingredient.dto';
import { Ingredient } from './entities/ingredient.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class IngredientService {
  constructor(
    @InjectRepository(Ingredient)
    private ingredientRepository: Repository<Ingredient>,
  ) {}

  create(createIngredientDto: CreateIngredientDto): Promise<Ingredient> {
    return this.ingredientRepository.save(createIngredientDto);
  }

  findAll(): Promise<Ingredient[]> {
    return this.ingredientRepository.find();
  }

  findOne(id: number) {
    return this.ingredientRepository.findOneBy({ id });
  }

  async update(id: number, updateIngredientDto: UpdateIngredientDto) {
    await this.ingredientRepository.update(id, updateIngredientDto);
    const ingredient = await this.ingredientRepository.findOneBy({ id });
    return ingredient;
  }

  async remove(id: number) {
    const deleteIngredient = await this.ingredientRepository.findOneBy({ id });
    return this.ingredientRepository.remove(deleteIngredient);
  }
}
