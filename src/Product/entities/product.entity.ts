import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  Id: number;
  @Column()
  ProductName: string;
  @Column()
  Price: number;
  @Column()
  Image: string;
}
