import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Ingredient {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  IngredientID: string;

  @Column()
  IngredientName: string;

  @Column()
  Minimum: number;

  @Column()
  Balance: number;

  @Column()
  Price: number;
}

// roles: ('admin' | 'user')[];
