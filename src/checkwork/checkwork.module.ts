import { Module } from '@nestjs/common';
import { CheckWorkService } from './checkwork.service';
import { CheckWorkController } from './checkwork.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CheckWork } from './entities/checkwork.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CheckWork])],
  controllers: [CheckWorkController],
  providers: [CheckWorkService],
})
export class CheckWorkModule {}
