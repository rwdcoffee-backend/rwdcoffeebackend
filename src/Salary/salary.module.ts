import { Module } from '@nestjs/common';
import { SalaryService } from './Salary.service';
import { SalaryController } from './salary.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { paySalary } from './entities/salary.entity';

@Module({
  imports: [TypeOrmModule.forFeature([paySalary])],
  controllers: [SalaryController],
  providers: [SalaryService],
})
export class SalaryModule {}
