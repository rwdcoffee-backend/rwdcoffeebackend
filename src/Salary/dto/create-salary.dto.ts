export class CreateSalaryDto {
  id: number;
  date: string;
  userid: number;
  workinghour: number;
  workrate: number;
}
