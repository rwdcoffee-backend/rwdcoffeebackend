import { Test, TestingModule } from '@nestjs/testing';
import { MemberController } from './Member.controller';
import { MemberService } from './member.service';

describe('UsersController', () => {
  let controller: MemberController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MemberController],
      providers: [MemberService],
    }).compile();

    controller = module.get<MemberController>(MemberController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
