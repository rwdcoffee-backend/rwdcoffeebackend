import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TemperatureModule } from './temperature/temperature.module';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { User } from './users/entities/user.entity';
import { CheckWorkModule } from './checkwork/checkwork.module';
import { CheckWork } from './checkwork/entities/checkwork.entity';
import { Ingredient } from './ingredient/entities/ingredient.entity';
import { IngredientModule } from './ingredient/ingredient.module';
import { Product } from './Product/entities/product.entity';
import { ProductModule } from './Product/product.module';
import { paySalary } from './Salary/entities/salary.entity';
import { SalaryModule } from './Salary/salary.module';
import { Member } from './member/entities/Member.entity';
import { MemberModule } from './member/member.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { Role } from './roles/entities/role.entity';
import { RolesModule } from './roles/roles.module';
import { Type } from './types/entities/type.entity';
import { TypesModule } from './types/types.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'mydb.sqlite',
      synchronize: true,
      entities: [
        User,
        CheckWork,
        Ingredient,
        Product,
        paySalary,
        Member,
        Role,
        Type,
      ],
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    RolesModule,
    TypesModule,
    TemperatureModule,
    UsersModule,
    CheckWorkModule,
    IngredientModule,
    ProductModule,
    SalaryModule,
    MemberModule,
  ],
  controllers: [AppController],
  providers: [AppService],
  exports: [],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
