import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { MemberService } from './member.service';
import { CreateMemberDto } from './dto/create-member.dto';
import { UpdateMemberDto } from './dto/update-member.dto';

@Controller('Member')
export class MemberController {
  constructor(private readonly MemberService: MemberService) {}
  // Create
  @Post()
  create(@Body() createUserDto: CreateMemberDto) {
    return this.MemberService.create(createUserDto);
  }

  // Read All
  @Get()
  findAll() {
    return this.MemberService.findAll();
  }
  // Read One
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.MemberService.findOne(+id);
  }
  // Partial Update
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateMemberDto) {
    return this.MemberService.update(+id, updateUserDto);
  }
  // Delete
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.MemberService.remove(+id);
  }
}
