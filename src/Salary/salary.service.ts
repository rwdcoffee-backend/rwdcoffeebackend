import { Injectable } from '@nestjs/common';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';
import { paySalary } from './entities/salary.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class SalaryService {
  constructor(
    @InjectRepository(paySalary)
    private SalaryRepository: Repository<paySalary>,
  ) {}

  create(createSalaryDto: CreateSalaryDto): Promise<paySalary> {
    return this.SalaryRepository.save(createSalaryDto);
  }

  findAll(): Promise<paySalary[]> {
    return this.SalaryRepository.find();
  }

  findOne() {
    return this.SalaryRepository.findOneBy({});
  }

  async update(id: number, updateSalaryDto: UpdateSalaryDto) {
    await this.SalaryRepository.update(id, updateSalaryDto);
    const Salary = await this.SalaryRepository.findOneBy({});
    return Salary;
  }

  async remove() {
    const deleteSalary = await this.SalaryRepository.findOneBy({});
    return this.SalaryRepository.remove(deleteSalary);
  }
}
